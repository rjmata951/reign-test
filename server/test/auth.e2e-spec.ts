import { HttpStatus } from '@nestjs/common';

import * as jwt from 'jsonwebtoken';
import * as request from 'supertest';
import { User } from '../src/auth';
import { mockUser } from './__testHelpers__';
import { RetrievedToken } from '../src/interfaces';
import { Schema, Types } from 'mongoose';

describe('AuthController (e2e)', () => {
  const authUrl = `http://localhost:3001/v1/auth/`;

  describe('/auth/signup (POST)', () => {
    it('it should register a user and return the JWT', () => {
      return request(authUrl)
        .post('/signup')
        .set('Accept', 'application/json')
        .send({ username: 'rjmata' })
        .expect((response: request.Response) => {
          const { access_token } = response.body;
          const tokenPayload = jwt.verify(access_token, process.env.SECRET_JWT);
          console.log(tokenPayload);
          // expect(tokenPayload.username).toEqual('rjmata');
          // expect(tokenPayload.sub).toBe(Types.ObjectId);
        })
        .expect(HttpStatus.CREATED);
    });
  });
});
