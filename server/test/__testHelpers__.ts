export const mockPostsService = {
  findAll: () => {
    return mockPosts;
  },
  findWithDiscards: (userId) =>
    mockPosts.filter((post) => post._id !== '613eb0a0f9a87ebda81b8b0f'),
};

export const mockUsersService = {
  insertDiscardedPost: ({ _id, discards }) => ({
    discards: [...discards],
  }),
  create: (username) => ({
    _id: '613e74966d772e95f73fdb0e',
    username,
  }),
};

export const mockAuthService = {
  createUser: (username) => ({
    _id: '613e74966d772e95f73fdb0e',
    username,
  }),
  validateUser: (username) => username === 'rjmata',
};

export const mockPost = {
  _id: '6143a240afad2a6f9c38fe1d',
  title: 'Kape Technologies buys ExpressVPN for $936M',
  createdAt: '2021-09-16T19:48:54.000Z',
  url: 'https://alternativeto.net/news/2021/9/kape-technologies-buys-expressvpn-for-936-million/',
  storyId: '28556533',
  __v: 0,
};
export const mockUser = {
  discards: [
    '613eb0a0f9a87ebda81b8b0f',
    '61401e404e4e58dc42d85fd0',
    '6140f1302508d44f4fe769bd',
  ],
  _id: '613e74966d772e95f73fdb0e',
  username: 'rjmata',
};
export const mockTokenPayload = {
  sub: '613e74966d772e95f73fdb0e',
  username: 'rjmata',
  iat: 1631821922,
  exp: 1631908322,
};
export const mockJWT = {
  access_token:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InJqbWF0YSIsInN1YiI6IjYxM2U3NDk2NmQ3NzJlOTVmNzNmZGIwZSIsImlhdCI6MTYzMTgyMTkyMiwiZXhwIjoxNjMxOTA4MzIyfQ.koBIbgh_RtGfr0hH1jaFpLMEQoaGYCp7SNGpg1eR6j8',
};
export const mockPosts = [
  {
    _id: '6143a240afad2a6f9c38fe1d',
    title: 'Kape Technologies buys ExpressVPN for $936M',
    createdAt: '2021-09-16T19:48:54.000Z',
    url: 'https://alternativeto.net/news/2021/9/kape-technologies-buys-expressvpn-for-936-million/',
    storyId: '28556533',
    __v: 0,
  },
  {
    _id: '613eb0a0f9a87ebda81b8b0f',
    title: 'COBOL – Still standing the test of time',
    createdAt: '2021-09-16T17:31:40.000Z',
    url: 'https://blog.microfocus.com/cobol-still-standing-the-test-of-time/',
    storyId: '28554607',
    __v: 0,
  },
  {
    _id: '61438620c7ea23525613ce61',
    title: 'Deno 1.14',
    createdAt: '2021-09-16T17:03:42.000Z',
    url: 'https://deno.com/blog/v1.14',
    storyId: '28554292',
    __v: 0,
  },
];
