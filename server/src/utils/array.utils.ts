export class ArrayUtils {
  static intoObject(array: Array<any>) {
    let newObject = {};
    array.forEach((elem: any): void => {
      newObject = {
        ...newObject,
        [elem]: true,
      };
    });
    return newObject;
  }

  static filterArray(array: Array<any>, obj = {}, prop: string) {
    return array.filter((elem: any) => {
      if (obj[elem[prop].toString()]) return false;
      return true;
    });
  }
  static modelArrayToJsonArray(array: Array<any>) {
    return array.map((modelItem) => modelItem.toJSON());
  }
}
