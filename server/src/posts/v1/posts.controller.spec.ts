import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import * as mockHttp from 'node-mocks-http';

import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';
import { Post } from '../schemas';
import {
  mockPosts,
  mockPostsService,
  mockPost,
  mockUser,
  mockTokenPayload,
  mockJWT,
} from '../../../test/__testHelpers__';
import { FilterPostsDto } from '../dtos/posts.dto';

describe('PostsController', () => {
  let controller: PostsController;
  const mockRequest = mockHttp.createRequest();
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PostsController],
      providers: [
        PostsService,
        {
          provide: getModelToken(Post.name),
          useValue: mockPost,
        },
      ],
    })
      .overrideProvider(PostsService)
      .useValue(mockPostsService)
      .compile();

    controller = module.get<PostsController>(PostsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should find all posts', () => {
    expect(controller.get(new FilterPostsDto())).toEqual(mockPosts);
  });

  it('should find posts without the discarded ones', () => {
    mockRequest.user = { sub: mockTokenPayload.sub };
    expect(controller.getFiltered(mockRequest, new FilterPostsDto())).toEqual(
      mockPosts.filter((post) => post._id !== '613eb0a0f9a87ebda81b8b0f'),
    );
  });
});
