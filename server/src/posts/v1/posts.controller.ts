import {
  Controller,
  Get,
  Query,
  Param,
  Post,
  Body,
  Put,
  Delete,
  HttpStatus,
  HttpCode,
  Res,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Response, Request } from 'express';
import { ApiTags, ApiOperation } from '@nestjs/swagger';

import { PostsService } from './posts.service';
import { FilterPostsDto } from '../dtos/posts.dto';
import { JwtGuard, Public } from '../../auth/';
import { RetrievedToken } from '../../interfaces';

@UseGuards(JwtGuard)
@ApiTags('posts')
@Controller('/v1/posts')
export class PostsController {
  constructor(private postsService: PostsService) {}

  @Public()
  @Get()
  get(@Query() params: FilterPostsDto) {
    return this.postsService.findAll(params);
  }

  @Get('user-discards')
  getFiltered(@Req() req: Request, @Query() params: FilterPostsDto) {
    const { sub: userId } = req.user as RetrievedToken;
    const result = this.postsService.findWithDiscards(userId, params);
    return result;
  }
}
