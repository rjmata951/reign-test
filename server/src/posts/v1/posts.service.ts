import {
  Inject,
  Injectable,
  // InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { HttpService } from '@nestjs/axios';
import { ConfigType } from '@nestjs/config';
import { Cron, CronExpression, Timeout } from '@nestjs/schedule';

import { CreatePostsDto, FilterPostsDto } from '../dtos/posts.dto';
import { Post } from '../schemas/posts.schema';
import { config } from '../../config';
import { User } from '../../auth/schemas/users.schema';
import { ArrayUtils } from '../../utils';

@Injectable()
export class PostsService {
  constructor(
    @InjectModel(Post.name) private postModel: Model<Post>,
    private httpService: HttpService,
    @Inject(config.KEY) private configService: ConfigType<typeof config>,
    @InjectModel(User.name) private userModel: Model<User>,
  ) {}

  findAll(params?: FilterPostsDto) {
    try {
      const { limit, offset, sort, desc } = params;
      const order = desc ? 'desc' : 'asc';
      return this.postModel
        .find()
        .sort({ [sort]: order })
        .skip(offset)
        .limit(limit)
        .exec();
    } catch (e) {
      console.log('error in findAll', e);
    }
  }

  async findWithDiscards(userId: Types.ObjectId, params?: FilterPostsDto) {
    const { discards } = await this.userModel.findById(userId).exec();
    const discardsObj = ArrayUtils.intoObject(discards);
    const posts: Array<Post> = await this.findAll(params);
    const postsData = ArrayUtils.modelArrayToJsonArray(posts);
    return ArrayUtils.filterArray(postsData, discardsObj, '_id');
  }

  @Cron(CronExpression.EVERY_HOUR)
  populatePosts() {
    this.httpService.get(this.configService.server.apiUrl).subscribe((val) => {
      const processedData: Post[] = this.processData(val.data.hits);
      try {
        this.postModel.insertMany(processedData, { ordered: false }, (error) =>
          error ? console.log(`couldn't insert posts: ${error}`) : null,
        );
      } catch (e) {
        console.log(`error in inserting new posts: ${e}`);
      }
    });
  }

  @Timeout(2000)
  firstPopulate() {
    this.populatePosts();
  }

  processData(dataArray): Post[] {
    return dataArray.reduce(
      (
        result,
        {
          title,
          story_title,
          comment_text,
          story_url,
          url,
          created_at,
          objectID,
          author,
        },
      ) => {
        if (!title && !story_title) return result;
        return [
          ...result,
          {
            title: title ? title : story_title,
            createdAt: new Date(created_at),
            url: url ? url : story_url,
            comment: comment_text,
            storyId: objectID,
            author,
          },
        ];
      },
      [],
    );
  }
}
