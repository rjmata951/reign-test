import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Post extends Document {
  @Prop({ required: true })
  createdAt: Date;

  @Prop()
  author: string;

  @Prop({ required: true })
  title: string;

  @Prop()
  url: string;

  @Prop()
  comments: string;

  @Prop({ required: true, unique: true })
  storyId: string;
}

export const PostSchema = SchemaFactory.createForClass(Post);
