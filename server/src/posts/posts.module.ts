import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HttpModule } from '@nestjs/axios';

import { PostsController, PostsService } from './v1';
import { Post, PostSchema } from './schemas/posts.schema';
import { User, UserSchema } from '../auth/schemas/users.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Post.name,
        schema: PostSchema,
      },
      {
        name: User.name,
        schema: UserSchema,
      },
    ]),
    HttpModule,
  ],
  controllers: [PostsController],
  providers: [PostsService],
})
export class PostsModule {}
