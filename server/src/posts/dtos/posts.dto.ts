import {
  IsString,
  IsUrl,
  IsDate,
  IsNotEmpty,
  IsOptional,
  IsPositive,
  Min,
  Equals,
  Max,
  IsBoolean,
} from 'class-validator';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';

export class CreatePostsDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly author: string;

  @IsDate()
  @IsNotEmpty()
  @ApiProperty()
  readonly createdAt: Date;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly title: string;

  @IsUrl()
  @IsNotEmpty()
  @ApiProperty()
  readonly url: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly comments: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly storyId: string;
}

export class UpdateProductDto extends PartialType(CreatePostsDto) {}

export class FilterPostsDto {
  @Type(() => Number)
  @IsOptional()
  @IsPositive()
  @Max(20)
  limit = 20;

  @Type(() => Number)
  @IsOptional()
  @Min(0)
  offset = 0;

  @IsOptional()
  @IsString()
  // @Equals('createdAt')
  sort = 'createdAt';

  @Type(() => Boolean)
  @IsOptional()
  @IsBoolean()
  // @Equals('createdAt')
  desc = false;
}
