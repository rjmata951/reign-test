import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import * as Joi from 'joi';

import { environments, config } from './config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PostsModule } from './posts';
import { DatabaseModule } from './database';
import { AuthModule } from './auth';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: environments[process.env.NODE_ENV] || '.env',
      load: [config],
      isGlobal: true,
      validationSchema: Joi.object({
        DB_NAME: Joi.string().required(),
        DB_PORT: Joi.number().required(),
        DB_USER: Joi.string().required(),
        DB_PW: Joi.string().required(),
        DB_HOST: Joi.string().required(),
        DB_DIALECT: Joi.string().required(),
        PORT: Joi.number().required(),
        SECRET_JWT: Joi.string().required(),
      }),
    }),
    ScheduleModule.forRoot(),
    PostsModule,
    AuthModule,
    DatabaseModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
