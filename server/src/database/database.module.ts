import { Module, Global } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';

import { config } from '../config';

@Global()
@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: (configService: ConfigType<typeof config>) => {
        const { dialect, user, password, host, port, dbName } =
          configService.database;
        return {
          uri: `${dialect}://${host}:$${port}/?authSource=admin&readPreference=primary`,
          user,
          pass: password,
          dbName,
          useUnifiedTopology: true,
          useNewUrlParser: true,
          useCreateIndex: true,
        };
      },
      inject: [config.KEY],
    }),
  ],
  exports: [MongooseModule],
})
export class DatabaseModule {}
