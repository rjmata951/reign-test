import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigType } from '@nestjs/config';

import {
  AuthService,
  AuthController,
  UsersController,
  UsersService,
} from './v1';
import { LocalStrategy, JwtStrategy } from './strategies/';
import { config } from '../config';
import { User, UserSchema } from './schemas/users.schema';
import { Post, PostSchema } from '../posts/schemas/posts.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: User.name,
        schema: UserSchema,
      },
      {
        name: Post.name,
        schema: PostSchema,
      },
    ]),
    PassportModule,
    JwtModule.registerAsync({
      useFactory: (configService: ConfigType<typeof config>) => {
        return {
          secret: configService.server.secretKey,
          signOptions: {
            expiresIn: '1d',
          },
        };
      },
      inject: [config.KEY],
    }),
  ],
  providers: [AuthService, UsersService, LocalStrategy, JwtStrategy],
  controllers: [AuthController, UsersController],
  exports: [UsersService],
})
export class AuthModule {}
