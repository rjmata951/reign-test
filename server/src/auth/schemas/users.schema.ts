import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

import { Post } from '../../posts/schemas/posts.schema';
import { Exclude } from 'class-transformer';

@Schema()
export class User extends Document {
  // @Exclude()
  @Prop({ required: true, unique: true })
  username: string;

  // @Prop({ required: true })
  // password: string;

  @Prop({ type: [{ type: Types.ObjectId, ref: Post.name }] })
  discards: Types.Array<Post>;
}

export const UserSchema = SchemaFactory.createForClass(User);
