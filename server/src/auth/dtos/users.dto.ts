import {
  IsString,
  IsNotEmpty,
  IsEmail,
  Length,
  IsArray,
  IsMongoId,
} from 'class-validator';
import { PartialType, ApiProperty } from '@nestjs/swagger';
import { Types } from 'mongoose';

export class InsertUserDiscardDto {
  @IsNotEmpty()
  @IsMongoId()
  @ApiProperty({ description: 'corresponding users id' })
  readonly _id: Types.ObjectId;

  @IsNotEmpty()
  @IsArray()
  @ApiProperty({ description: 'posts the user has discarded' })
  readonly discards: string[];
}

export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: "the users' username" })
  readonly username: string;
}

// export class CreateUserDto extends PartialType(InsertUserDiscardDto) {}
export class UpdateUserDto extends PartialType(InsertUserDiscardDto) {}
