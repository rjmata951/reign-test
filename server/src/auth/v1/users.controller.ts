import {
  Body,
  Controller,
  Get,
  Post,
  Param,
  Query,
  Delete,
  Put,
  HttpStatus,
  HttpCode,
  UseGuards,
  UseInterceptors,
  Req,
  ClassSerializerInterceptor,
} from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { Request } from 'express';

import { UsersService } from './users.service';
import { CreateUserDto, InsertUserDiscardDto } from '../dtos/users.dto';
import { JwtGuard } from '../guards/jwt.guard';
import { RetrievedToken } from '../../interfaces';

@UseGuards(JwtGuard)
@ApiTags('users')
@Controller('/v1/users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  // @Get()
  // // @ApiOperation({})
  // get() {
  //   return this.usersService.findAll();
  // }

  // @Get(':userId')
  // // @ApiOperation({})
  // getById(@Param('userId') userId: string) {
  //   return this.usersService.findById(userId);
  // }

  // @Post()
  // create(@Body() payload: CreateUserDto) {
  //   return this.usersService.create(payload);
  // }

  @Put('/discards')
  @ApiOperation({ summary: 'adds posts to discard array' })
  addDiscardedPost(@Req() req: Request, @Body() { discards }) {
    const { sub: _id } = req.user as RetrievedToken;
    return this.usersService.insertDiscardedPost({ _id, discards });
  }
}
