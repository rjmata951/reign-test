import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { CreateUserDto, InsertUserDiscardDto } from '../dtos/users.dto';
import { User } from '../schemas/users.schema';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}

  async create(payload: CreateUserDto) {
    try {
      const newUser = new this.userModel(payload);
      return await newUser.save();
    } catch (e) {
      return null;
    }
  }

  // async findAll() {
  //   return await this.userModel.find().exec();
  // }

  async findByUsername(username: string) {
    return await this.userModel.findOne({ username }).exec();
  }
  //
  // async findById(userId: string) {
  //   return await this.userModel.findById(userId).exec();
  // }

  async insertDiscardedPost({ _id, discards }: InsertUserDiscardDto) {
    const user = await this.userModel.findById(_id).exec();
    discards.forEach((discard) => user.discards.push(discard));
    const result = await user.save();
    const { _id: userId, username, ...rest } = result.toJSON();
    return rest;
  }
}
