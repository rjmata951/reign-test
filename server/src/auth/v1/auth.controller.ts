import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import { CreateUserDto } from '../dtos/users.dto';
import { User } from '../schemas/users.schema';
import { AuthService } from './auth.service';

@ApiTags('auth')
@Controller('/v1/auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post('signup')
  create(@Body() payload: CreateUserDto) {
    return this.authService.createUser(payload);
  }
  @UseGuards(AuthGuard('local'))
  @Post('login')
  login(@Req() req: Request) {
    const user = req.user as User;
    return this.authService.generateJwt(user);
  }
}
