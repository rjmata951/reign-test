import * as mockHttp from 'node-mocks-http';
import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';

import { FilterPostsDto, PostsController, PostsService } from '../../posts';
import { Post } from '../../posts/schemas';
import { User } from '../schemas/users.schema';
import {
  mockPost,
  mockPosts,
  mockUsersService,
  mockTokenPayload,
  mockUser,
  mockJWT,
} from '../../../test/__testHelpers__';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtService } from '@nestjs/jwt';

describe('UsersController', () => {
  let controller: UsersController;
  const mockRequest = mockHttp.createRequest({
    body: { discards: mockUser.discards },
  });
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        UsersService,
        {
          provide: AuthService,
          useValue: jest.fn().mockImplementation(() => true),
        },
        {
          provide: JwtService,
          useValue: jest.fn().mockImplementation(() => mockJWT),
        },
        {
          provide: getModelToken(Post.name),
          useValue: mockPost,
        },
        {
          provide: getModelToken(User.name),
          useValue: mockUser,
        },
      ],
    })
      .overrideProvider(UsersService)
      .useValue(mockUsersService)
      .compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should insert discarded posts all posts', () => {
    mockRequest.user = { sub: mockTokenPayload.sub };
    expect(controller.addDiscardedPost(mockRequest, mockRequest.body)).toEqual({
      discards: mockUser.discards,
    });
  });
});
