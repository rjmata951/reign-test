import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from '../dtos/users.dto';
import { User } from '../schemas/users.schema';
import { UsersService } from './users.service';
import { PayloadToken } from '../../interfaces/';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async createUser(username: CreateUserDto) {
    const user = await this.usersService.create(username);
    if (!user)
      throw new HttpException(
        'this username already exists',
        HttpStatus.FORBIDDEN,
      );
    const payload: PayloadToken = { username: user.username, sub: user._id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async validateUser(username: string) {
    const user = await this.usersService.findByUsername(username);
    if (!user) return null;
    return user;
  }

  generateJwt({ username, _id }: User) {
    const payload: PayloadToken = { username, sub: _id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
