import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
// import { Strategy } from 'passport-local';
import { Strategy } from 'passport-local-token';

import { AuthService } from '../v1';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(private authService: AuthService) {
    super({
      tokenField: 'username',
    });
  }
  async validate(username: string) {
    const user = await this.authService.validateUser(username);
    if (!user) throw new UnauthorizedException('authentication failed');
    return user;
  }
}
