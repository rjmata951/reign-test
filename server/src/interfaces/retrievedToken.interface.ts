import { Types } from 'mongoose';
import { Request } from 'express';

export interface RetrievedToken extends Request {
  username: string;
  sub: Types.ObjectId;
  iat: number;
  exp: number;
}
