import { registerAs } from '@nestjs/config';

export const config = registerAs('config', () => {
  return {
    database: {
      dbName: process.env.DB_NAME,
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT, 10),
      user: process.env.DB_USER,
      password: process.env.DB_PW,
      dialect: process.env.DB_DIALECT,
    },
    server: {
      port: process.env.PORT,
      apiUrl: process.env.API_URL,
      secretKey: process.env.SECRET_JWT,
    },
  };
});
