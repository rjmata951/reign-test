# Reign-test

Quick Test to opt for a Full-stack developer position at Reign

## Getting started
Assuming you already have docker and docker-compose installed in your PC.

In case you don't...
https://docs.docker.com/compose/gettingstarted/

Now, make sure you have the corresponding files downloaded, i.e, Git clone or download the files.

https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html

## Run the project

Now that you're all set,

There are two ways you could run this project.


1. run it in development mode
2. run it in production mode

The former would imply performing a npm install on both server and client directories,
but also you would have to set up a docker container for your mongo DB. 

but first let's go the easy way (the number 2).

all you gotta do is open your terminal on the root directory of this project (where the docker-compose.yml lies),
and type:

$ docker-compose up -d

then, docker is gonna do its thing. 

to open the client, go to localhost:3000/ in your browser and sign up (since it's most likely your first time using this app)

the backend is exposed in port 3001.
backend documentation isn't complete, but is exposed at /docs

looking forward to your thoughts about this project.


...

there is also the 1st option, not the easiest though.

you would have to do the following to set up the mongodb container:

create a docker-compose.yml somewhere out of this project directory, that contains the following info:

image: mongo

container_name: mongodb
ports:"27017:27017"
environment:
MONGO_INITDB_ROOT_USERNAME: root
MONGO_INITDB_ROOT_PASSWORD: example
MONGO_INITDB_DATABASE: reign-test


PLEASE NOTICE THAT, like python, indentation is important. Please refer to this link to properly set up your container

https://hub.docker.com/_/mongo


