module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ['plugin:react/recommended', 'eslint:recommended'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: ['react'],
  rules: {
    'react/prop-types': ['off'],
    'react/jsx-props-no-spreading': ['off'],
    'no-console': ['off'],
    'react/button-has-type': ['off'],
  },
  ignorePatterns: ['.eslintrc.js', 'node_modules/**.js'],
};
