const envVar = {
    API_URL: "http://localhost:3001",
    LOGIN_ENDPOINT: "v1/auth/login",
    SIGNUP_ENDPOINT: "v1/auth/signup",
    POSTS_ENDPOINT: "v1/posts/user-discards",
    DISCARDS_ENDPOINT: "v1/users/discards",
};
export default envVar;