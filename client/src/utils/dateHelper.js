export const getRelativeTime = (arrayOfObjects) => {
  const currentTime = new Date();
  const currentDate = currentTime.getDate();
  const currentMonth = currentTime.getMonth();
  const currentYear = currentTime.getFullYear();
  return arrayOfObjects.map(({ createdAt, ...rest }) => {
    const parsedDate = new Date(createdAt);
    return {
      ...rest,
      createdAt: dateFormatted(
        parsedDate,
        currentYear,
        currentMonth,
        currentDate,
      ),
    };
  });
};

const formatAMPM = (date) => {
  let hours = date.getHours();
  let minutes = date.getMinutes();
  let ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes;
  const strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
};

const formatDay = (dateToTest, currentDate) => {
  const date = dateToTest.getDate();
  const day = currentDate - date;
  switch (day) {
    case 0:
      return formatAMPM(dateToTest);
    case 1:
      return 'Yesterday';
    default:
      return `${months[dateToTest.getMonth()]} ${date}`;
  }
};

const dateFormatted = (dateToTest, currentYear, currentMonth, currentDate) => {
  if (currentYear - dateToTest.getFullYear() > 0)
    return dateToTest.getFullYear();
  if (currentMonth - dateToTest.getMonth() > 0) {
    return `${months[dateToTest.getMonth()]} ${dateToTest.getDate()}`;
  } else {
    return formatDay(dateToTest, currentDate);
  }
};

const months = {
  0: 'January',
  1: 'February',
  2: 'March',
  3: 'April',
  4: 'May',
  5: 'June',
  6: 'July',
  7: 'August',
  8: 'September',
  9: 'October',
  10: 'November',
  11: 'December',
};
