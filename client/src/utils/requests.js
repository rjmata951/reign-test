import axios from 'axios';

import envVar from "./constants";

const {
    API_URL,
    LOGIN_ENDPOINT,
    SIGNUP_ENDPOINT,
    POSTS_ENDPOINT,
    DISCARDS_ENDPOINT
} = envVar;

export const logIn = async (credentials) => {
    const response = await axios.post(
        `${API_URL}/${LOGIN_ENDPOINT}`,
        credentials,
    );
    localStorage.setItem('jwt', response.data.access_token);
};

export const signUp = async (credentials) => {
    const response = await axios.post(
        `${API_URL}/${SIGNUP_ENDPOINT}`,
        credentials,
    );
    localStorage.setItem('jwt', response.data.access_token);
};

export const fetchProducts = (token) => {
    return axios.get(`${API_URL}/${POSTS_ENDPOINT}?desc=true`, {
        headers: {Authorization: `Bearer ${token}`},
    });
};

export const discardProducts = (token, productsId) => {
    return axios.put(
        `${API_URL}/${DISCARDS_ENDPOINT}`,
        {discards: productsId},
        {
            headers: {Authorization: `Bearer ${token}`},
        },
    );
};
