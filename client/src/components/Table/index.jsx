import React from 'react';

import {
  TableCell,
  TableContainer,
  Typography,
  DeleteButton,
  TableRow,
} from '../../styles';

const Table = ({ arrayOfValues, onDelete }) => {
  return (
    <TableContainer>
      {arrayOfValues.map(({ title, author, createdAt, url = '', _id }) => (
        <TableRow key={_id}>
          <TableCell href={url} target="_blank">
            <Typography style={{ maxWidth: '50%' }}>{title}</Typography>
            <Typography dimmed>-{author}-</Typography>
            <Typography style={{ marginLeft: 'auto' }}>{createdAt}</Typography>
          </TableCell>
          <DeleteButton onClick={() => onDelete(_id)} />
        </TableRow>
      ))}
    </TableContainer>
  );
};

export default Table;
