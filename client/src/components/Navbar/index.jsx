import React from 'react';
import { NavbarContainer, Toolbar, Logo } from '../../styles/Navbar';

const Navbar = () => (
  <NavbarContainer>
    <Toolbar>
      <Logo>
        <h1>HN Feed</h1>
        <h6>We &lt;3 Hacker news</h6>
      </Logo>
    </Toolbar>
  </NavbarContainer>
);

export default Navbar;
