import React from 'react';
import { DialogContainer, Typography } from '../../styles';

const Error = ({ error }) => {
  return (
    <DialogContainer mini>
      <Typography style={{ color: '#fc0349' }}>{error}</Typography>
    </DialogContainer>
  );
};
export default Error;
