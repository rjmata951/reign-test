import styled from 'styled-components';
import React from 'react';

const NoSubmitForm = styled.form`
  font-size: 2em;
  font-weight: bold;
  color: #fff;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Form = (props) => (
  <NoSubmitForm onSubmit={(e) => e.preventDefault()} {...props} />
);
export const Input = styled.input`
  margin: 1em;
  padding: 1em 1.5em;
  border: ${(props) => props.theme.borderColor};
  border-radius: 12px;
  box-shadow: 0px 5px 6px 0px rgba(0, 0, 0, 0.59);

  &:focus {
    outline: none;
    box-shadow: 0px 5px 6px 0px rgba(0, 0, 0, 0.9);
  }
`;
