import styled from 'styled-components';

export const Typography = styled.p`
  color: ${({ theme, dimmed }) => (dimmed ? theme.dimmedFontColor : theme.primaryColor)};
  margin: 0 0.5em;
`;
export const TableCell = styled.a`
  display: flex;
  flex-grow: 1;
  align-items: center;
  padding: 2em 1em;
  text-decoration: none;
  color: inherit;
`;
export const TableRow = styled.div`
  display: flex;
  align-items: center;
  border-bottom: 0.0625rem solid ${((props) => props.theme.borderColor)};
  &:hover {
    background-color: #fafafa;
    cursor: pointer;
  };
  
`;

export const TableContainer = styled.div`
  width: 100%;
  //overflow: auto;
`;
