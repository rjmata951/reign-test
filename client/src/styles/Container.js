import styled from 'styled-components';

export const Container = styled.div`
  color: ${(props) => props.theme.primaryColor};
  background-color: ${(props) => props.theme.bgColor};
  max-width: 95%;
  margin: 0 auto;
  font-size: 0.8125em;
`;

export const DialogContainer = styled.div`
  color: ${(props) => props.theme.primaryColor};
  background-color: ${(props) => props.theme.dimmedFontColor};
  width: ${(props) => (props.mini ? '20%' : '50%')};
  height: ${(props) => (props.mini ? '10vh' : '25vh')};
  margin: 0 auto;
  border-radius: 25px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  box-shadow: 0px 5px 6px 0px rgba(0, 0, 0, 0.59);
`;
