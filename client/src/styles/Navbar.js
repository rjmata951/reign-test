import styled from 'styled-components';

export const NavbarContainer = styled.div`
  background-color: ${(props) => props.theme.primaryColor};
  width: 100%;
  padding: 4em 2em;
  color: ${(props) => props.theme.bgColor};
  position: sticky;
  top: 0;
  left: auto;
  right: 0;
  margin-bottom: 2em;
  box-shadow: 0px 5px 6px 0px rgba(0,0,0,0.59);
  z-index: 1000;
`;

export const Toolbar = styled.nav`
  max-width: 80%;
  display: flex;
`;

export const Logo = styled.div`
  font-size: 3em;
`;
