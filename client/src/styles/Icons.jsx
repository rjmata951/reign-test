import React from 'react';
import styled from 'styled-components';

export const DeleteButton = (props) => (
  <Icons {...props}>
    <i className="fas fa-trash-alt fa-2x" />
  </Icons>
);

export const Spinner = (props) => (
  <StyledWrapper {...props}>
    <i className="fas fa-sync fa-spin fa-5x" style={{ margin: '0 auto ' }} />
  </StyledWrapper>
);

export const StyledWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 50%;
  height: 50vh;
  margin: auto 0;
`;

export const Icons = styled.button`
  margin: 1em;
  border: none;
  &:hover {
    cursor: pointer;
  }
`;
