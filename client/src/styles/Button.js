import styled from 'styled-components';

export const Button = styled.button`
  &:hover {
    cursor: pointer;
    opacity: 30%;
    background-color: ${(props) => props.theme.primaryColor};
    color: ${(props) => props.theme.dimmedFontColor};
  }
  padding: 0.5em 1.5em;
  border: ${(props) => props.theme.borderColor};
  border-radius: 12px;
`;

export const TypoButton = styled.button`
  &:hover {
    cursor: pointer;
    opacity: 30%;
    background-color: ${(props) => props.theme.bgColor};
    color: ${(props) => props.theme.primaryColor};
  }
  padding: 0.25em 1em;
  border: ${(props) => props.theme.borderColor};
  border-radius: 12px;
  background-color: transparent;
  margin: 1em 0;
`;
