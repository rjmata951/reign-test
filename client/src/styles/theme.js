const theme = {
  primaryColor: '#333333',
  bgColor: '#fff',
  borderColor: '#ccc',
  dimmedFontColor: '#999',
};
export default theme;
