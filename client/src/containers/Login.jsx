import React, { useState } from 'react';

import { Typography, Button, TypoButton, DialogContainer } from '../styles';
import { logIn, signUp } from '../utils/requests';
import { Form, Input } from '../styles/Form';
import Error from '../components/Error';
import { formInitialState, initialState } from '../utils/initialState';

const Login = ({ history }) => {
  const [state, setState] = useState(initialState);
  const [formValue, setFormValue] = useState(formInitialState);
  const handleLogin = (e) => {
    e.preventDefault();
    logIn(formValue)
      .then(() => history.push('/home'))
      .catch(() =>
        setState((state) => ({
          ...state,
          error: 'user invalid',
        })),
      );
  };
  const handleSignup = () => {
    signUp(formValue)
      .then(() => history.push('/home'))
      .catch(() =>
        setState((state) => ({
          ...state,
          error: 'user invalid',
        })),
      );
  };
  const handleChange = (e) => {
    setFormValue({
      username: e.target.value.trim(),
    });
  };
  return (
    <>
      <DialogContainer>
        <Form>
          <label htmlFor="username">
            <Typography>Username</Typography>
          </label>
          <Input
            required
            id="username"
            type="text"
            value={formValue.username}
            autoComplete="off"
            onChange={handleChange}
          />
        </Form>
        {state.login ? (
          <Button onClick={handleSignup}>Sign up</Button>
        ) : (
          <Button onClick={handleLogin}>Log in</Button>
        )}
        <TypoButton
          onClick={() =>
            setState(({ login, ...rest }) => ({ ...rest, login: !login }))
          }
        >
          {state.login ? 'Already a user? Sign in' : 'Not a User?'}
        </TypoButton>
      </DialogContainer>
      {state.error && <Error error={state.error} />}
    </>
  );
};

export default Login;
