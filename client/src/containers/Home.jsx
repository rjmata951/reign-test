import React, { useEffect, useState } from 'react';

import Table from '../components/Table';
import { Spinner } from '../styles';
import { discardProducts, fetchProducts } from '../utils/requests';
import { getRelativeTime } from '../utils/dateHelper';

const Home = () => {
  const [values, setValues] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  // const [error, setError] = useState(false);
  useEffect(() => {
    if (isLoading) {
      (async () => {
        try {
          const jwt = localStorage.getItem('jwt');
          const response = await fetchProducts(jwt);
          const processedTime = getRelativeTime(response.data);
          setValues(processedTime);
        } catch (e) {
          console.log(`Error encountered: ${e}`);
          // setError(true);
        } finally {
          setIsLoading(false);
        }
      })();
    }
  }, []);

  const deleteHandler = async (id) => {
    try {
      setIsLoading(true);
      const response = await discardProducts(localStorage.getItem('jwt'), [id]);
      if (response.status === 200)
        setValues(values.filter((item) => item._id !== id));
    } catch (e) {
      console.log(`error: ${e}`);
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <>
      {isLoading ? (
        <Spinner />
      ) : (
        <Table arrayOfValues={values} onDelete={deleteHandler} />
      )}
    </>
  );
};

export default Home;
