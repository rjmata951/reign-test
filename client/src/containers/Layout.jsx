import React from 'react';
import { Container } from '../styles/Container';
import Navbar from '../components/Navbar';

const Layout = ({ children }) => (
  <>
    <Navbar />
    <Container>{children}</Container>
  </>
);

export default Layout;
