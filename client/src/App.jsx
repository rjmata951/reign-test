import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import Home from './containers/Home';
import NotFound from './containers/NotFound';
import Layout from './containers/Layout';
import Login from './containers/Login';

function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route component={Login} path="/" exact />
          <Route component={Home} path="/home" exact />
          <Route component={NotFound} />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
